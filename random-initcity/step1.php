<?php
require '../inc/loader.php';

// Initiate Client communication
if (!isset($_SESSION['client']))
{
	$_SESSION['client'] = new Api_Client(API_CLIENT_KEY, API_CLIENT_DOMAIN);
}

// Start Client session
$foundCities = array();
$error = '';
if (isset($_GET['city']) && strlen($_GET['city']) >= 2)
{
	$request = $_SESSION['client']->init_city($_GET['city']);

	switch ($request->body('status'))
	{
		case '001':
			header('Location: ./step2.php'); exit;
			break;
		case '002':
			$foundCities = $request->body('cities');
			break;
		case '003':
			$error = 'Can\'t find a city. Please, search again.';
			break;
		case '004':
			$error = 'Can\'t find any advertisements for this city. Please, search again.';
			break;
		case '101': case '102':
			echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
			break;

	}
}

require '../html/head.php';
?>
<?php
if (isset($error))
{
	echo '<p class="error">'.$error.'</p>';
}
?>

<h2>Step 1 - Search for a city</h2>
<form action="./step1.php" method="get">
	<div class="form-control">
		<label>Search for city</label>
		<input type="text" name="city" value="<?php echo (isset($_GET['city']))?$_GET['city']:''; ?>" />

		<input type="submit" name="submit" value="Search" />
	</div>

	<?php
	if (count($foundCities) > 0)
	{
	?>
		<p>
			<strong>Multiple cities found:</strong><br />
			<ul>
			<?php
			foreach ($foundCities AS $city)
			{
			?>
				<li><a href="./step1.php?city=<?php echo $city; ?>"><?php echo $city; ?></a></li>
			<?php
			}
			?>
			</ul>
		</p>
	<?php
	}
	?>
</form>

<?php
require '../html/bottom.php';
?>