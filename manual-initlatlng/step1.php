<?php
require '../inc/loader.php';

// Initiate Client communication
if (!isset($_SESSION['client']))
{
	$_SESSION['client'] = new Api_Client(API_CLIENT_KEY, API_CLIENT_DOMAIN);
}

// Categories list (you might want to cache these, they don't change often)
$request = $_SESSION['client']->categories();
$categories = $request->body('categories');
$amountOfCategories = $request->body('amount');

// After form submit
$error = '';
if (isset($_GET['latlng']) && strlen($_GET['latlng']) >= 2)
{
	$request = $_SESSION['client']->init_latlng($_GET['latlng'], ((isset($_GET['category_id']))?$_GET['category_id']:0), 'manual');

	switch ($request->body('status'))
	{
		case '001':
			header('Location: ./step2.php'); exit;
			break;
		case '002':
			$error = 'Can\'t find a city for these latitude and longitude.';
			break;
		case '003':
			$error = 'Can\'t find any advertisements for this city. Please, search again.';
			break;
		case '101': case '102':
			echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
			break;

	}
}

require '../html/head.php';
?>

<?php
if (isset($error))
{
	echo '<p class="error">'.$error.'</p>';
}
?>

<h2>Step 1 - Search for a latitude and longitude</h2>
<form action="./step1.php" method="get">
	<div class="form-control">
		<?php
		if ($amountOfCategories > 0)
		{
		?>
			<label>Categorie</label><br />
			<select name="category_id">
			<?php
			foreach ($categories AS $categoryID => $categoryName)
			{
			?>
				<option value="<?php echo $categoryID; ?>"><?php echo $categoryName; ?></option>
			<?php
			}
			?>
			</select><br />
		<?php
		}
		?>
		<label>Search for latitude and longitude</label><br />
		<input type="text" name="latlng" value="<?php echo (isset($_GET['latlng']))?$_GET['latlng']:'53.219383,6.566502'; ?>" placeholder="53.219383,6.566502" /><br /><br />

		<input type="submit" name="submit" value="Search" />
	</div>
</form>

<?php
require '../html/bottom.php';
?>