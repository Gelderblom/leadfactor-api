<?php
require '../inc/loader.php';

// Initiate Client communication
if (!isset($_SESSION['client']))
{
	$_SESSION['client'] = new Api_Client(API_CLIENT_KEY, API_CLIENT_DOMAIN);
}

// Reset errors
$error = '';
$errors = array();

// Custom fields set-up
$externalId = NULL; // Optional: Set your own external ID if you want to use this function
$customFields = NULL; // Optional: Set your own custom fields if you want to use this function, custom fields can als be added/changed outside the user´s session with your API key and external ID

// Post form field
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$request = $_SESSION['client']->quote($_POST['data'], $externalId, $customFields);
	switch ($request->body('status'))
	{
		case '001':
			unset($_SESSION['client']);
			header('Location: ./step4.php'); exit;
			break;
		case '002':
			$error = 'Verplicht veld ontbreekt. Controleer uw invoer.';
			$errors = $request->body('errors');
			break;
		case '003':
			$error = 'Geen plaats gevonden.';
			break;
		case '004':
			$error = 'Geen advertenties geselecteerd.';
			break;
		case '101':
			echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
			break;
		case '102':
			echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
			break;
		case '105':
			echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
			break;
	}
}

// Find form fields
$request = $_SESSION['client']->fields();
switch ($request->body('status'))
{
	case '101':
		echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
		break;
}

require '../html/head.php';

if (isset($error))
{
	echo '<p class="error">'.$error.'</p>';
}
?>

<h2>Step 3 - Client details</h2>
<form action="./step3.php" method="post">
	<div class="form-control">
		<?php
		foreach ($request->body('fields') AS $field)
		{
			if ($field->type == 'category')
			{
				echo '<h3>'.$field->label.'</h3>';
			}
			else
			{
				echo '<label'.((in_array($field->id, $errors))?' class="label-error"':'').'>'.$field->label.(($field->required == 1)?' *':'').'</label><br />';
				if ($field->type == 'text')
				{
					echo '<input type="text" name="data['.$field->id.']" value="'.((isset($_POST['data'][$field->id]))?$_POST['data'][$field->id]:$field->default).'" /><br />';
				}
				elseif ($field->type == 'textarea')
				{
					echo '<textarea name="data['.$field->id.']" rows="4" cols="50">'.((isset($_POST['data'][$field->id]))?$_POST['data'][$field->id]:$field->default).'</textarea><br />';
				}
				elseif ($field->type == 'select')
				{
					echo '<select name="data['.$field->id.']">';
					foreach ($field->options AS $option)
					{
						echo '<option'.(((isset($_POST['data'][$field->id]) && $_POST['data'][$field->id] == $option) || (!isset($_POST['data'][$field->id]) && $option == $field->default))?$_POST['data'][$field->id]:'').'>'.$option.'</option>';
					}
					echo '</select><br />';
				}
			}
		}
		?>

		<br />
		<input type="submit" name="submit" value="Verstuur offerte aanvaag" />
	</div>
</form>

<?php
require '../html/bottom.php';
?>