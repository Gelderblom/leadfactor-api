<?php
require '../inc/loader.php';

// Initiate Client communication
if (!isset($_SESSION['client']))
{
	$_SESSION['client'] = new Api_Client(API_CLIENT_KEY, API_CLIENT_DOMAIN);
}

// Reset errors
$error = '';
$errors = array();

// Post form field
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$request = $_SESSION['client']->advertisements($_POST['advertisement']);

	switch ($request->body('status'))
	{
		case '001':
			header('Location: ./step3.php'); exit;
			break;
		case '002':
			$error = 'Te weinig advertenties geselecteerd.';
			break;
		case '003':
			$error = 'Te veel advertenties geselecteerd.';
			break;
		case '101':
			echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
			break;
		case '102':
			echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
			break;
	}
}

// Found advertisements
$request = $_SESSION['client']->lastRequest();
switch ($request->body('status'))
{
	case '101':
		echo 'Error '.$request->body('status').' found, read the documentation for more information.'; exit;
		break;
}

require '../html/head.php';
?>

<?php
if (isset($error))
{
	echo '<p class="error">'.$error.'</p>';
}
?>

<h2>Step 2 - Advertentie kiezen</h2>
<form action="./step2.php" method="post">
	<div class="form-control">
		<?php
		foreach ($request->body('advertisements') AS $advertisement)
		{
			echo '<input type="checkbox" name="advertisement[]" value="'.$advertisement->advertisementId.'" checked="checked" /> '.$advertisement->drivingSchoolName.'<br />';
		}
		?>

		<br />
		<input type="submit" name="submit" value="Advertenties selecteren" />
	</div>
</form>

<?php
require '../html/bottom.php';
?>