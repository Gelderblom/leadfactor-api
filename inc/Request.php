<?php
/**
 * Provides remote server communications options using [curl](http://php.net/curl).
 */
class Client_Request {

	private $url;
	private $fields;
	private $auth;

	private $body;
	private $headers;

	public function __construct($url, $fields = array(), $auth = false)
	{
		$this->url = $url;
		$this->fields = $fields;
		$this->auth = $auth;
	}

	public function execute()
	{
		$curl = curl_init($this->url);
		curl_setopt($curl, CURLOPT_USERAGENT, "Leadfactor API v.2 curl agent");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		curl_setopt($curl, CURLOPT_HEADER, 1);

		if ($this->auth)
		{
			curl_setopt($curl, CURLOPT_USERPWD, "$this->auth");
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		}

		if ($this->fields)
		{
			$fields_string = http_build_query($this->fields);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);
		}

		$response = curl_exec($curl);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header_string = substr($response, 0, $header_size);
		$body = substr($response, $header_size);

		$header_rows = explode(PHP_EOL, $header_string);
		$header_rows = array_filter($header_rows, 'trim');
		$i = 0;
		$headers = array();
		foreach ((array)$header_rows as $hr)
		{
			$colonpos = strpos($hr, ':');
			$key = $colonpos !== false ? substr($hr, 0, $colonpos) : (int)$i++;
			$headers[$key] = $colonpos !== false ? trim(substr($hr, $colonpos+1)) : $hr;
		}
		$j = 0;
		foreach ((array)$headers as $key => $val)
		{
			$vals = explode(';', $val);
			if (count($vals) >= 2)
			{
				unset($headers[$key]);
				foreach ($vals as $vk => $vv)
				{
					$equalpos = strpos($vv, '=');
					$vkey = $equalpos !== false ? trim(substr($vv, 0, $equalpos)) : (int)$j++;
					$headers[$key][$vkey] = $equalpos !== false ? trim(substr($vv, $equalpos+1)) : $vv;
				}
			}
		}
		//print_rr($headers);
		curl_close($curl);

		$this->body = json_decode($body);
		$this->headers = $headers;

		return $this;
	}

	public function body($key = '')
	{
		if (strlen($key) > 0)
		{
			if (isset($this->body->$key))
			{
				return $this->body->$key;
			}
			return '';
		}
		return $this->body;
	}

}
