<?php
/*// Set error reporting for debugging
error_reporting(E_ALL); # http://nl3.php.net/error_reporting
ini_set("display_errors", 1);
ini_set("track_errors", 1);
ini_set("html_errors", 1);*/

// Configs
define('API_CLIENT_PATH', '../inc/');
define('API_CLIENT_KEY', '');
define('API_CLIENT_DOMAIN', 'domeinnaam.nl');

// Require client classes
include API_CLIENT_PATH.'Client.php';

// Start sessions
session_start();
?>