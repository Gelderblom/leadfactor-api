<?php
// Core configurations, to be able to adapt to future changes
define('API_CORE_PROTOCOL', 'https://');
define('API_CORE_SUBDOMAIN', 'api.');
define('API_CORE_DOMAIN', 'leadfactor.nl');
define('API_CORE_REMOTE_PATH', '/');

// Request class
require API_CLIENT_PATH.'Request.php';

/**
 * API client class
 */
class Api_Client {
	/**
	 * Your API key
	 *
	 * @var string
	 */
	private $apiKey;
	/**
	 * Branche website
	 *
	 * @var string
	 */
	private $apiDomain;
	/**
	 * Session key, to track user progress
	 *
	 * @var string
	 */
	private $sessionKey = '';
	/**
	 * Response data
	 *
	 * @var mixed
	 */
	private $data = array();
	/**
	 * Last request object
	 *
	 * @var Client_Request
	 */
	private $lastRequest = NULL;

	/**
	 * Construct
	 *
	 * @param string Your API key
	 * @param string Branche website
	 */
	public function __construct($apiKey, $apiDomain)
	{
		$this->apiKey = $apiKey;
		$this->apiDomain = $apiDomain;
	}

	/**
	 * Categories request to API
	 *
	 * @return Client_Request
	 */
	public function categories()
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;

		$request = new Client_Request($this->buildURL('categories/'), $this->data);
		$request->execute();

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Init city request to API
	 *
	 * @param string City to search
	 * @param enum{random|manual} Type of advertisement selection
	 * @param int Category ID (required if branche website has categories, else send 0)
	 *
	 * @return Client_Request
	 */
	public function init_city($city, $type = 'random', $category_id = 0)
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;
		if (strlen($this->sessionKey) > 0)
		{
			$this->data['session_key'] = $this->sessionKey;
		}
		$this->data['city'] = $city;
		$this->data['type'] = $type;
		$this->data['category_id'] = $category_id;

		$request = new Client_Request($this->buildURL('init-city/'), $this->data);
		$request->execute();

		$this->sessionKey = $request->body('session_key');

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Init latlng request to API
	 *
	 * @param string latitude,longitude to search
	 * @param enum{random|manual} Type of advertisement selection
	 * @param int Category ID (required if branche website has categories, else send 0)
	 *
	 * @return Client_Request
	 */
	public function init_latlng($latlng, $type = 'random', $category_id = 0)
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;
		if (strlen($this->sessionKey) > 0)
		{
			$this->data['session_key'] = $this->sessionKey;
		}
		$this->data['latlng'] = $latlng;
		$this->data['type'] = $type;
		$this->data['category_id'] = $category_id;

		$request = new Client_Request($this->buildURL('init-latlng/'), $this->data);
		$request->execute();

		$this->sessionKey = $request->body('session_key');

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Advertisements request to API
	 *
	 * @param int[] Advertisement ID's
	 *
	 * @return Client_Request
	 */
	public function advertisements($advertisementsIds)
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;
		$this->data['session_key'] = $this->sessionKey;
		$this->data['advertisement_ids'] = $advertisementsIds;

		$request = new Client_Request($this->buildURL('advertisements/'), $this->data);
		$request->execute();

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Fields request to API
	 *
	 * @return Client_Request
	 */
	public function fields()
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;

		$request = new Client_Request($this->buildURL('fields/'), $this->data);
		$request->execute();

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Quote request to API
	 *
	 * @param mixed Input from user form
	 * @param int External ID to connect
	 * @param mixed Custom fields
	 *
	 * @return Client_Request
	 */
	public function quote($fields, $externalId = NULL, $customFields = NULL)
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;
		$this->data['session_key'] = $this->sessionKey;
		$this->data['fields'] = $fields;
		if ($externalId !== NULL)
		{
			$this->data['external_id'] = $externalId;
		}
		if ($customFields !== NULL)
		{
			$this->data['custom_fields'] = $customFields;
		}

		$request = new Client_Request($this->buildURL('quote/'), $this->data);
		$request->execute();

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Set external ID request to API
	 *
	 * @param string Session key
	 * @param int External ID
	 *
	 * @return Client_Request
	 */
	public function set_external_id($sessionKey, $externalId)
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;
		$this->data['session_key'] = $sessionKey;
		$this->data['external_id'] = $externalId;

		$request = new Client_Request($this->buildURL('set-external-id/'), $this->data);
		$request->execute();

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Set custom fields request to API
	 *
	 * @param int External ID
	 * @param mixed Custom fields
	 *
	 * @return Client_Request
	 */
	public function set_custom_fields($externalId, $customFields)
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;
		$this->data['external_id'] = $externalId;
		$this->data['custom_fields'] = $customFields;

		$request = new Client_Request($this->buildURL('set-custom-fields/'), $this->data);
		$request->execute();

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Get custom fields request to API
	 *
	 * @param int External ID
	 *
	 * @return Client_Request
	 */
	public function get_custom_fields($externalId)
	{
		$this->data = array();
		$this->data['site'] = $this->apiDomain;
		$this->data['api_key'] = $this->apiKey;
		$this->data['external_id'] = $externalId;

		$request = new Client_Request($this->buildURL('get-custom-fields/'), $this->data);
		$request->execute();

		$this->lastRequest = $request;

		return $request;
	}

	/**
	 * Build URL with all configuration options
	 *
	 * @param string URL to API method
	 *
	 * @return string Full URL
	 */
	private function buildURL($url)
	{
		return API_CORE_PROTOCOL.API_CORE_SUBDOMAIN.API_CORE_DOMAIN.API_CORE_REMOTE_PATH.$url;
	}

	/**
	 * Return last request value
	 *
	 * @return Client_Request
	 */
	public function lastRequest()
	{
		return $this->lastRequest;
	}
}