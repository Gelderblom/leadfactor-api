<?php
require '../inc/loader.php';

// Initiate Client communication
if (!isset($_SESSION['client']))
{
	$_SESSION['client'] = new Api_Client(API_CLIENT_KEY, API_CLIENT_DOMAIN);
}

// Set message
$message = '';

// Post request
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	// Set external ID form
	if (isset($_POST['type']) && $_POST['type'] == 'set_external_Id')
	{
		// Send request
		$request = $_SESSION['client']->set_external_id($_POST['session_key'], $_POST['external_id']);
		// Status code handler
		switch ($request->body('status'))
		{
			case '001':
				$message = 'Succesvol opgeslagen';
				break;
			case '002':
				$message = 'Nog geen aanvrager aan sessie gekoppeld. Is de aanvraag afgerond?';
				break;
			case '003':
				$message = 'Externe ID ontbreekt';
				break;
			case '004':
				$message = 'Externe ID bestaat al';
				break;
			case '101': case '102':
				$message = 'Error '.$request->body('status').' found, read the documentation for more information.';
				break;
		}
	}
	elseif (isset($_POST['type']) && $_POST['type'] == 'get_custom_fields')
	{
		// Send request
		$request = $_SESSION['client']->get_custom_fields($_POST['external_id']);
		// Status code handler
		switch ($request->body('status'))
		{
			case '001':
				$message = 'Succesvol opgehaald, waarde (json format):<br /><pre>'.json_encode($request->body('custom_fields')).'</pre>';
				break;
			case '103':
				$message = 'Externe ID bestaat niet';
				break;
			case '101':
				$message = 'Error '.$request->body('status').' found, read the documentation for more information.';
				break;
		}
	}
	elseif (isset($_POST['type']) && $_POST['type'] == 'set_custom_fields')
	{
		// Send request
		$request = $_SESSION['client']->set_custom_fields($_POST['external_id'], array($_POST['fieldKey'] => $_POST['fieldValue']));
		// Status code handler
		switch ($request->body('status'))
		{
			case '001':
				$message = 'Succesvol opgeslagen';
				break;
			case '002':
				$message = 'Custom fields waarde ontbreekt';
				break;
			case '103':
				$message = 'Externe ID bestaat niet';
				break;
			case '101': case '102':
				$message = 'Error '.$request->body('status').' found, read the documentation for more information.';
				break;
		}
	}
}

// HTML head
require '../html/head.php';

// Show message
if (isset($message) && strlen($message) > 0)
{
	echo '<p class="message">'.$message.'</p>';
}
?>
<h2>Connect external ID to an user</h2>
<form action="./index.php" method="post">
	<input type="hidden" name="type" value="set_external_Id">
	<div class="form-control">
		<label>Session key (zie <a href="http://watkosttest.nl/api2beheer/api-session/" target="_blank">watkosttest.nl/api2beheer/api-session/</a>)</label><br />
		<input type="text" name="session_key" value="f60b9fc305c7e4e0ef42a4c7a6f3efd8" /><br />

		<label>Your ID (external ID)</label><br />
		<input type="text" name="external_id" value="555" /><br /><br />

		<input type="submit" name="submit" value="Connect" />
	</div>
</form>

<h2>Retreive custom fields for external ID</h2>
<form action="./index.php" method="post">
	<input type="hidden" name="type" value="get_custom_fields">
	<div class="form-control">
		<label>Your ID (external ID)</label><br />
		<input type="text" name="external_id" value="555" /><br /><br />

		<input type="submit" name="submit" value="Connect" />
	</div>
</form>

<h2>Change custom field for external ID</h2>
<form action="./index.php" method="post">
	<input type="hidden" name="type" value="set_custom_fields">
	<div class="form-control">
		<label>Your ID (external ID)</label><br />
		<input type="text" name="external_id" value="555" /><br /><br />
		<label>Field name</label><br />
		<input type="text" name="fieldKey" value="subscribed" /><br /><br />
		<label>Field value</label><br />
		<input type="text" name="fieldValue" value="1" /><br /><br />

		<input type="submit" name="submit" value="Connect" />
	</div>
</form>

<?php
// HTML bottom
require '../html/bottom.php';
?>