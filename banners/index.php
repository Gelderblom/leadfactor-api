<?php
$cdn = 'https://cdn.leadfactor.nl/';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
        <title>Leadfactor - Banner demo</title>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
        <link href="../assets/style.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <div class="container">
            <h1>Demo banners</h1>

            <p>Onderstaande banners zijn eenvoudig te plaatsen op iedere gewenste plek op uw website.</p>

            <p>De banners hebben jQuery nodig om te functioneren. Maakt u nog geen gebruik van jQuery, plaats deze dan binnen uw website.</p>
            <xmp>
<head>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
            </xmp>

            <p>Daarna kunt u de javascript module toevoegen.</p>
            <xmp>
<head>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://cdn.leadfactor.nl/banner/latest.js"></script>
</head>
            </xmp>

            <h2>Banner 300px x 250px</h2>
            <div id="leadfactor-banner-1"></div>
            <p>
                Voorbeeld code:
            </p>
            <xmp>
<div id="leadfactor-banner-1"></div>
<script type="text/javascript">
$(function()
{
    leadfactor.init({
        selector: '#leadfactor-banner-1', // jQuery selector van container element
        width: '300', // Breedte, volgens voorgedefinieerde formaten
        height: '250', // Hoogte, volgens voorgedefinieerde formaten
        apiKey: '{api sleutel hier}', // De verkregen API sleutel
        site: '{website hier, bijv: watkosttest.nl}', // De domeinnaam van de desbetreffende website

        text_title: 'Vergelijk :keyword', // Text: Titel
        text_keywordslabel: 'Vul uw plaatsnaam in', // Text: Plaatsnamen label
        text_keywordsplaceholder: 'Bijv. Amsterdam', // Text: Plaatsnamen placeholder
        text_categorylabel: 'Selecteer categorie', // Text: Categorie label
        text_submit: 'Ga verder', // Text: Submit button
        text_keyword: 'hoveniers' // Text: Vervangt :keyword voor deze waarde
    });
});
</script>
            </xmp>

            <h2>Banner 728px x 160px</h2>
            <div id="leadfactor-banner-2"></div>
            <xmp>
<div id="leadfactor-banner-2"></div>
<script type="text/javascript">
$(function()
{
    leadfactor.init({
        selector: '#leadfactor-banner-2', // jQuery selector van container element
        width: '728', // Breedte, volgens voorgedefinieerde formaten
        height: '160', // Hoogte, volgens voorgedefinieerde formaten
        apiKey: '{api sleutel hier}', // De verkregen API sleutel
        site: '{website hier, bijv: watkosttest.nl}', // De domeinnaam van de desbetreffende website

        text_title: 'Vergelijk :keyword', // Text: Titel
        text_keywordslabel: 'Vul uw plaatsnaam in', // Text: Plaatsnamen label
        text_keywordsplaceholder: 'Bijv. Amsterdam', // Text: Plaatsnamen placeholder
        text_categorylabel: 'Selecteer categorie', // Text: Categorie label
        text_submit: 'Ga verder', // Text: Submit button
        text_keyword: 'hoveniers' // Text: Vervangt :keyword voor deze waarde
    });
});
</script>
            </xmp>

            <h2>Banner 160px x 600px</h2>
            <div id="leadfactor-banner-3"></div>
            <xmp>
<div id="leadfactor-banner-3"></div>
<script type="text/javascript">
$(function()
{
    leadfactor.init({
        selector: '#leadfactor-banner-3', // jQuery selector van container element
        width: '160', // Breedte, volgens voorgedefinieerde formaten
        height: '600', // Hoogte, volgens voorgedefinieerde formaten
        apiKey: '{api sleutel hier}', // De verkregen API sleutel
        site: '{website hier, bijv: watkosttest.nl}', // De domeinnaam van de desbetreffende website

        text_title: 'Vergelijk :keyword', // Text: Titel
        text_keywordslabel: 'Vul uw plaatsnaam in', // Text: Plaatsnamen label
        text_keywordsplaceholder: 'Bijv. Amsterdam', // Text: Plaatsnamen placeholder
        text_categorylabel: 'Selecteer categorie', // Text: Categorie label
        text_submit: 'Ga verder', // Text: Submit button
        text_keyword: 'hoveniers' // Text: Vervangt :keyword voor deze waarde
    });
});
</script>
            </xmp>

            <h2>Banner 300px x 600px</h2>
            <div id="leadfactor-banner-4"></div>
            <xmp>
<div id="leadfactor-banner-4"></div>
<script type="text/javascript">
$(function()
{
    leadfactor.init({
        selector: '#leadfactor-banner-4', // jQuery selector van container element
        width: '300', // Breedte, volgens voorgedefinieerde formaten
        height: '600', // Hoogte, volgens voorgedefinieerde formaten
        apiKey: '{api sleutel hier}', // De verkregen API sleutel
        site: '{website hier, bijv: watkosttest.nl}', // De domeinnaam van de desbetreffende website

        text_title: 'Vergelijk :keyword', // Text: Titel
        text_keywordslabel: 'Vul uw plaatsnaam in', // Text: Plaatsnamen label
        text_keywordsplaceholder: 'Bijv. Amsterdam', // Text: Plaatsnamen placeholder
        text_categorylabel: 'Selecteer categorie', // Text: Categorie label
        text_submit: 'Ga verder', // Text: Submit button
        text_keyword: 'hoveniers' // Text: Vervangt :keyword voor deze waarde
    });
});
</script>
            </xmp>

            <h2>Banner 970px x 250px</h2>
            <div id="leadfactor-banner-5"></div>
            <xmp>
<div id="leadfactor-banner-5"></div>
<script type="text/javascript">
$(function()
{
    leadfactor.init({
        selector: '#leadfactor-banner-5', // jQuery selector van container element
        width: '970', // Breedte, volgens voorgedefinieerde formaten
        height: '250', // Hoogte, volgens voorgedefinieerde formaten
        apiKey: '{api sleutel hier}', // De verkregen API sleutel
        site: '{website hier, bijv: watkosttest.nl}', // De domeinnaam van de desbetreffende website

        text_title: 'Vergelijk :keyword', // Text: Titel
        text_keywordslabel: 'Vul uw plaatsnaam in', // Text: Plaatsnamen label
        text_keywordsplaceholder: 'Bijv. Amsterdam', // Text: Plaatsnamen placeholder
        text_categorylabel: 'Selecteer categorie', // Text: Categorie label
        text_submit: 'Ga verder', // Text: Submit button
        text_keyword: 'hoveniers' // Text: Vervangt :keyword voor deze waarde
    });
});
</script>
            </xmp>

            <h2>Banner generator</h2>
            <p>Bovenstaande banner formaten kunnen ook gegenereerd worden. Gebruik hiervoor onderstaand formulier. Vergeet niet de module uit de eerste stap ook toe te voegen.</p>
            <form name="generator" id="generator">
                <label>Selector</label><br />
                <input type="text" name="id" value="" placeholder="leadfactor-banner" /><br />
                <label>Formaat</label><br />
                <select name="size">
                    <option value="300x250">300px x 250px</option>
                    <option value="728x160">728px x 160px</option>
                    <option value="160x600">160px x 600px</option>
                    <option value="300x600">300px x 600px</option>
                    <option value="970x250">970px x 250px</option>
                </select><br />
                <label>API sleutel</label><br />
                <input type="text" name="key" value="" placeholder="Verkregen API sleutel" /><br />
                <label>Website</label><br />
                <select name="website">
                    <option>watkosttest.nl</option>
                    <option>studentrijbewijs.nl</option>
                    <option>watkosteenverhuisbedrijf.nl</option>
                    <option>watkosteenpianoverhuizen.nl</option>
                </select><br />
                <label>Titel</label><br />
                <input type="text" name="text_title" value="Vergelijk :keyword" /><br />
                <label>Plaatsnamen label</label><br />
                <input type="text" name="text_keywordslabel" value="Vul uw plaatsnaam in" /><br />
                <label>Plaatsnamen placeholder</label><br />
                <input type="text" name="text_keywordsplaceholder" value="Bijv. Amsterdam" /><br />
                <label>Categorie label</label><br />
                <input type="text" name="text_categorylabel" value="Selecteer categorie" /><br />
                <label>Submit button</label><br />
                <input type="text" name="text_submit" value="Ga verder" /><br />
                <label>Vervangt :keyword voor deze waarde</label><br />
                <input type="text" name="text_keyword" value="hoveniers" /><br />

                <xmp id="generator-results-template" style="display: none;">
<div id="{id}"></div>
<script type="text/javascript">
$(function()
{
    leadfactor.init({
        selector: '{id}', // jQuery selector van container element
        width: '{width}', // Breedte, volgens voorgedefinieerde formaten
        height: '{height}', // Hoogte, volgens voorgedefinieerde formaten
        apiKey: '{key}', // De verkregen API sleutel
        site: '{website}', // De domeinnaam van de desbetreffende website

        text_title: '{text_title}', // Text: Titel
        text_keywordslabel: '{text_keywordslabel}', // Text: Plaatsnamen label
        text_keywordsplaceholder: '{text_keywordsplaceholder}', // Text: Plaatsnamen placeholder
        text_categorylabel: '{text_categorylabel}', // Text: Categorie label
        text_submit: '{text_submit}', // Text: Submit button
        text_keyword: '{text_keyword}' // Text: Vervangt :keyword voor deze waarde
    });
});
</script>
</xmp>
                <xmp id="generator-results"></xmp>
            </form>
            <script type="text/javascript">
            $(function()
            {
                $('#generator input, #generator select').on('change', function()
                {
                    var code = $('#generator-results-template').text();
                    code = code.replace('{id}', $('input[name=id]').val());
                    code = code.replace('{id}', '#'+$('input[name=id]').val());
                    var width = 0;
                    var height = 0;
                    if ($('select[name=size]').val().length > 0)
                    {
                        var size = $('select[name=size]').val().split('x');
                        width = size[0];
                        height = size[1];
                    }
                    code = code.replace('{width}', width);
                    code = code.replace('{height}', height);
                    code = code.replace('{key}', $('input[name=key]').val());
                    code = code.replace('{website}', $('select[name=website]').val());
                    code = code.replace('{text_title}', $('input[name=text_title]').val());
                    code = code.replace('{text_keywordslabel}', $('input[name=text_keywordslabel]').val());
                    code = code.replace('{text_keywordsplaceholder}', $('input[name=text_keywordsplaceholder]').val());
                    code = code.replace('{text_categorylabel}', $('input[name=text_categorylabel]').val());
                    code = code.replace('{text_submit}', $('input[name=text_submit]').val());
                    code = code.replace('{text_keyword}', $('input[name=text_keyword]').val());
                    $('#generator-results').text(code);
                });
                $('#generator input[name=id]').trigger('change');
            });
            </script>


            <br /><br /><hr /><br /><br />
            <ul>
                <li><a href="../index.php">Demo overzicht</a></li>
            </ul>
        </div>

        <script type="text/javascript" src="<?php echo $cdn; ?>banner/latest.js"></script>
        <script type="text/javascript">
        $(function()
        {
            leadfactor.init({
                selector: '#leadfactor-banner-1',
                width: '300',
                height: '250',
                apiKey: 'f20ff4be6c31a98140e16eba68e390d0',
                site: 'watkosteenhovenier.nl'
            });

            leadfactor.init({
                selector: '#leadfactor-banner-2',
                width: '728',
                height: '90',
                apiKey: 'f20ff4be6c31a98140e16eba68e390d0',
                site: 'watkosteenhovenier.nl'
            });

            leadfactor.init({
                selector: '#leadfactor-banner-3',
                width: '160',
                height: '600',
                apiKey: 'f20ff4be6c31a98140e16eba68e390d0',
                site: 'watkosttest.nl'
            });
            leadfactor.init({
                selector: '#leadfactor-banner-4',
                width: '300',
                height: '600',
                apiKey: 'f20ff4be6c31a98140e16eba68e390d0',
                site: 'watkosttest.nl'
            });
            leadfactor.init({
                selector: '#leadfactor-banner-5',
                width: '970',
                height: '250',
                apiKey: 'f20ff4be6c31a98140e16eba68e390d0',
                site: 'watkosttest.nl'
            });
        });
        </script>
    </body>
</html>