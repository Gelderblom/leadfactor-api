<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<title>Watkost API demo</title>
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		<link href="../assets/style.css" rel="stylesheet" media="screen">
	</head>
	<body>
		<div class="container">
			<h1>Demo PHP API client</h1>
			<ul>
				<li><a href="./random-initcity/">Random gekozen advertenties, gezocht op plaatsnaam.</a></li>
				<li><a href="./manual-initlatlng/">Handmatig gekozen advertenties, gezocht op latitude en longitude. Optioneel: In step3.php external_id en custom_fields koppeling.</a></li>
				<li><a href="./customfields/">Custom fields aanpassen met behulp van de external_id.</a></li>
			</ul>
			<h1>Demo Javascript banners</h1>
			<ul>
				<li><a href="./banners/">Random gekozen advertenties, gezocht op plaatsnaam.</a></li>
			</ul>
		</div>
	</body>
</html>